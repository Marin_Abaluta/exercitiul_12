package GenericsTwo;

public class GenTypes<T1,T2> {

    private T1 name;
    private T2 value;

    GenTypes(T1 name,T2 value  ){

        this.name = name;
        this.value = value;

    }



    public T1 getName() {
        return name;
    }

    public T2 getValue() {
        return value;
    }
}
